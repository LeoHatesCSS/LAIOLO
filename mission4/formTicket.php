<?php
	require("recupTicket.php");
	creatBaseTicket();
?>
			
<!DOCTYPE html> 
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Incident Ticket</title>
	</head>

	<body>
		<div class="body">
			<h1>
				Incident ticket form
			</h1>
			<form method="POST" action="<?php $_PHP_SELF?>" class="border rounded">
				<div class="login margin-1">
					<h2>Connection</h2>
				
	  				<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="login" placeholder="Enter email">
						<small id="email" class="form-text text-muted">We'll never share your email with anyone else.</small>
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
					</div>
				</div>

				<div class="ticket margin-1">
					<h2>Ticket</h2>

					<div class="form-group">
						<label for="titleTicket">Name your incident ticket</label>
						<input type="text" name= "sujet" class="form-control" id="titleTicket" required placeholder="Pink Bean escaped!">
					</div>

					<div class="form-group">
						<label for="emergencyLvl-list" class="form-label">Emergency level</label>
						<ul id="emergencyLvl-list" class="list-group list-group-horizontal-sm">
							<li class="list-group-item">0 : It's OK, it can wait.</li>
							<li class="list-group-item range-middle">	
								<input type="range" name= "prio" class="form-range" id="emergencyLvl" value="4" max="10" min="0" step="1" list="oklvl" aria-describedby="rangeHelp1" required>
								<datalist id="oklvl">
									<option value="0" label="It's OK, it can wait.">
									<option value="10" label="APOCALYPSE IS ON ITS WAY!!!">
								</datalist>
							</li>
							<li class="list-group-item">10 : APOCALYPSE IS ON ITS WAY!!!</li>
						</ul>
					</div>

					<div class="form-group">
						<label for="descripTicket" name="description">Description of the incident</label>
						<textarea name= "description" class="form-control" id="descripTicket" required placeholder="Pink Bean flied above the fences with kids' balloons!"></textarea>
					</div>

					<div class="form-group">
						<label for="statusTicket">Status</label>
						<input type="text"  class="form-control" id="statusTicket" required placeholder="ongoing?" name="statut">
					</div>

					<span>Zoo sector</span>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="secteur" id="Radio1" value="Normal mob" checked>
  						<label class="form-check-label" for="Radio1">
    						Normal monsters zone
  						</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="secteur" id="Radio2" value="Area boss">
						<label class="form-check-label" for="Radio2">
						    Area bosses zone
						</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="secteur" id="Radio3" value="Major boss">
						<label class="form-check-label" for="Radio3">
						    Major bosses zone
						</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="secteur" id="Radio4" value="Legendary">
						<label class="form-check-label" for="Radio4">
						    Legendary bosses zone
						</label>
					</div>
				
				</div>
				<button type="submit" name="submit" class="btn btn-primary margin-1">Submit</button>
			   	<a href="afficheListeTickets.php" class="btn btn-success margin-1">List of tickets</a>
<?php
	recupererTicket();	
?>
			</form>
		</div>
	</body>
</html>