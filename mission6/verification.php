<!DOCTYPE html> 
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Authentification</title>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
		<link rel="stylesheet" href="ticket.css">
	</head>

	<body class="body">
	<?php
        
    session_start();
        
    include("initDB.php");
        

	function verifpswd($id,$pswrd,$users) {
		//recherche via la base de données
        if (!userTableExist()) { //on vérifie l'existence d'une base de données. Si il n'y en a pas, on essaie de la créer.
            creatBas();
        }
        if (userTableExist()) { //si la base de données existe, on cherche l'utilisateur dedans
            $db=connectDB();
            $sql = "SELECT id FROM user WHERE email=:email AND password=:password;";
            $results=$db->prepare($sql);
            $email=$id;
            $password=$pswrd;
            $results->bindParam(":email", $email);
            $results->bindParam(":password", $password);
            $results->execute();
            $row=$results->fetch(PDO::FETCH_ASSOC);
            if ($row!=null) {
                $sql = "UPDATE `user` SET `registration`=:date WHERE `user`.`id`=:id; ";
                $results=$db->prepare($sql);
                $date=date("Y-m-d H:i:s");
                $id=$row['id'];
                $results->bindParam(":date", $date);
                $results->bindParam(":id", $id);
                $results->execute();
                $key=count($users->logs);
                return($key);
            }
        }
        
        //recherche dans le json en local
        $n = array();
        for ($i=0; $i<count($users->logs); $i++) {
            $n[$i]=$users->logs[$i]->email;
        }
		$key=array_search($id,$n);
		// echo $id;
		// echo "<br>".$pswrd;
		// echo "<br>".count($users);
		if (($key!==false)&&($key<count($users->logs))) {
			if (strcmp($users->logs[$key]->mdp,$pswrd)==0) {
				return($key);
			}
			else{
				return(null);
			}
		}
		else{
				return(null);
		}
	}
        
    function recupUtilLog ($nomfichier) {
        $utilisateurs= new class {};
        if (file_exists('login.json')) {
            $content=file_get_contents('login.json');
			$utilisateurs=json_decode($content);
        } else {
            $utilisateurs->logs= array();
            $utilisateurs->logs[0]=new class {}; 
            $utilisateurs->logs[0]->email="Lina@gmail.com";
            $utilisateurs->logs[0]->mdp="passeLina";
            file_put_contents('login.json', json_encode($utilisateurs));
        }
        return($utilisateurs);
    }
        
        

    if ((!isset($_POST['login']))||(!isset($_POST['pswrd']))||($_POST['login']==null)||($_POST['pswrd']==null)) {
        $key=null;
    } else {
       $utilisateurs=recupUtilLog("login.json");
        $key=verifpswd($_POST['login'],$_POST['pswrd'],$utilisateurs);
        
        if ($key==count($utilisateurs->logs)) { //cela signifie que l'utilisateur était enregistré dans la base de données et n'était pas forcément dans le fichier login.json .
            //afin de réutiliser notre travail précédent, on ajoute simplement l'utilisateur à l'objet $utilisateurs .
            $utilisateurs->logs[$key]=new class {};
            $utilisateurs->logs[$key]->email=$_POST['login'];
            $utilisateurs->logs[$key]->mdp=$_POST['pswrd'];
        }
    }
	if (gettype($key)=="integer") {
		//echo "abc";
		$_SESSION['loginUser']=$utilisateurs->logs[$key]->email;
		echo "<br />connected as ".$_SESSION['loginUser'];
		$content= new stdClass();
		$content->logs=array();
		if (file_exists('long.log')) {
			$content=file_get_contents('long.log');
			$content=json_decode($content);
			if ((isset($content->logs))&&($content->logs!==null)) {
				$index=count($content->logs);
                $content->logs[$index]= new stdClass();
				$content->logs[$index]->email=$utilisateurs->logs[$key]->email;
				$content->logs[$index]->mdp=$utilisateurs->logs[$key]->mdp;
				$content->logs[$index]->date=time();
			}
		} else {
            $content->logs[0]= new stdClass();
			$content->logs[0]->email=$utilisateurs->logs[$key]->email;
            $content->logs[0]->mdp=$utilisateurs->logs[$key]->mdp;
			$content->logs[0]->date=time();
		}
		//var_dump($content);
		file_put_contents('long.log', json_encode($content));
	} 
	else {
	?>
		<div class="alert alert-danger" role="alert">
			Wrong ID or password!
		</div>
	<?php
	}

	?>
	</body>
</html>