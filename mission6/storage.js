
function saveInSession(item, name) {
	sessionStorage.setItem(name, item);
	var token = sessionStorage.getItem(name);
}

function saveInLocal (item, name) {
	localStorage.setItem(name, item);
	var username = localStorage.getItem(name);
}

function removeFromSession(itemName) {
	sessionStorage.removeItem(itemName);
}

function clearLocal () {
	localStorage.clear();
}

console.log(sessionStorage);
console.log(localStorage);