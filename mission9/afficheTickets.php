<?php 
	require("recupTicket.php");
	$conn=connectDB();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ticket's information</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
		<link rel="stylesheet" href="ticket.css">
		<script src="afficheTickets.js"></script>
</head>
<body>
<div class="container">
<table class="table table-bordered">
	<h1>Ticket's information</h1>
	<thead>
		<tr>
			<td bgcolor="#E6E6FA">ID</td>
			<td bgcolor="#E6E6FA">Date</td>
			<td bgcolor="#E6E6FA">Login</td>
			<td bgcolor="#E6E6FA">Subject</td>
			<td bgcolor="#E6E6FA">Description</td>
			<td bgcolor="#E6E6FA">Priority</td>
			<td bgcolor="#E6E6FA">Zoo sector</td>
			<td bgcolor="#E6E6FA">Status</td>
			<td bgcolor="#E6E6FA">Your action</td>
		</tr>
	</thead>
	<tbody>
	
	<?php 
	if (isset($_GET['view'])){
		$id = $_GET['view'];
		$select="SELECT * FROM ticket where id = $id";
		$query=$conn->query($select);
		while($data=$query->fetch(PDO::FETCH_ASSOC))
		{
	?>
		<tr>
			<td id="idTicket"><?php echo $data["id"];?></td>
			<td><?php echo $data["datet"];?></td>
			<td><?php echo $data["login"];?></td>
			<td><?php echo $data["sujet"];?></td>
			<td><?php echo $data["description"];?></td>
			<td><?php echo $data["prio"];?></td>
			<td><?php echo $data["secteur"];?></td>
			<td id="maj"><?php echo $data["statut"];?><button id="resolved">Resolved ?</button></td>
			<td><a href="modifierTicket.php?modif=<?php echo $data["id"];?>" class="btn btn-primary">Edit this ticket</td>
		</tr>
	<?php
		}
	}
	?>
	</tbody>
</table>
    
</div>
</body>
</html>