<?php

include("initDB.php");

$conn=connectDB();


if (isset($_GET['id'])) {
?>
    <table class="table table-bordered">
    <thead>
		<tr>
			<td bgcolor="#E6E6FA">ID</td>
			<td bgcolor="#E6E6FA">Date</td>
			<td bgcolor="#E6E6FA">Login</td>
			<td bgcolor="#E6E6FA">Subject</td>
			<td bgcolor="#E6E6FA">Description</td>
			<td bgcolor="#E6E6FA">Priority</td>
			<td bgcolor="#E6E6FA">Zoo sector</td>
			<td bgcolor="#E6E6FA">Status</td>
			<td bgcolor="#E6E6FA">Your action</td>
		</tr>
	</thead>
	<tbody>
	
	<?php 
		$select="SELECT * FROM ticket WHERE id=:id;";
        $request=$conn->prepare($select);
        $request->bindParam(":id",$_GET['id']);
		$request->execute();
		while($data=$request->fetch(PDO::FETCH_ASSOC))
		{
	?>
		<tr>
			<td><?php echo $data["id"];?></td>
			<td><?php echo $data["datet"];?></td>
			<td><?php echo $data["login"];?></td>
			<td><?php echo $data["sujet"];?></td>
			<td><?php echo $data["description"];?></td>
			<td><?php echo $data["prio"];?></td>
			<td><?php echo $data["secteur"];?></td>
			<td><?php echo $data["statut"];?></td>
			<td><a href="afficheTickets.php?view=<?php echo $data["id"];?>" class="btn btn-success">View this ticket</td>
		</tr>
	<?php
		}
	?>
	</tbody>
</table>  
<?php
} else if (isset($_POST['id'])) {
    $select="UPDATE ticket SET statut='Resolved' WHERE id=:id;";
    $request=$conn->prepare($select);
    $request->bindParam(":id",$_POST['id']);
    $request->execute();
}

?>