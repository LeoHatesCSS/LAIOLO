--
-- Base de données :  `zootickoon`
--

-- --------------------------------------------------------

--
-- Structure de la table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
CREATE TABLE IF NOT EXISTS `ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datet` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `login` varchar(50) NOT NULL,
  `sujet` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  `prio` varchar(10) NOT NULL DEFAULT 'faible',
  `secteur` varchar(10) NOT NULL,
  `statut` varchar(10) NOT NULL DEFAULT 'en cours',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ticket`
--

INSERT INTO `ticket` (`id`, `datet`, `login`, `sujet`, `description`, `prio`, `secteur`, `statut`) VALUES
(1, '2020-07-30 14:37:49', 'zorg@zorg', 'fuite d\'eau', 'fuite d\'eau dans le local des lions', 'faible', 'savanne', 'en cours');
COMMIT;
