<!DOCTYPE html> 
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Registration</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
		<link rel="stylesheet" href="ticket.css">
        <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
        <script src="user.js"></script>
        <script src="storage.js"></script>
	</head>

	<body>
        
        <?php
        
        include("initDB.php");
	include("bandeau.html");
        
        function userExist($mail) {
            if (!userTableExist()) { //on vérifie l'existence d'une base de données. Si il n'y en a pas, on essaie de la créer.
                creatBas();
            }
            if (userTableExist()) {
                $db=connectDB();
                $sql = "SELECT count(id) FROM user WHERE email=:email;";
                $results=$db->prepare($sql);
                $results->bindParam(":email", $mail);
                $results->execute();
                $user=$results->fetch(PDO::FETCH_ASSOC);
                return($user["count(id)"]);
            }
            return(0);
        }
        
        function register($mail, $password) {
            if (userExist($mail)<1) {
                $db=connectDB();
                $sql = "INSERT INTO `user` (`id`, `email`, `password`, `registration`) VALUES (NULL, :email, :password, :date);";
                $results=$db->prepare($sql);
                $date=date("Y-m-d H:i:s");
                $results->bindParam(":email", $mail);
                $results->bindParam(":password", $password);
                $results->bindParam(":date", $date);
                $bool=$results->execute();
                if (userExist($mail)>=1) {
                    return(1);
                }
                return($bool);
            }
            return(0);
        }
        
        $pswd = "";
        $mail = "myemail@mail.mail";
        if (isset($_POST)) {
            if ((isset($_POST['login']))&&(isset($_POST['password']))&&(isset($_POST['confirmation']))) {
                $registration=2;
                if ($_POST['password']==$_POST['confirmation']) {
                    $registration=register($_POST['login'], $_POST['password']);
                    $pswd = $_POST['password'];
                }
                $mail = $_POST['login'];
            }
        }
        
        ?>
        
		<div class="body">
			<h1>
				Registration form
			</h1>
			<form method="POST" class="border rounded" action="
                        <?php 
                            $_PHP_SELF
                        ?>
            ">
				<div class="login margin-1">
					<h2>Put your informations</h2>
                    
	  				<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="login" placeholder="Enter email" value="
                        <?php
                            echo $mail;
                        ?>
                        ">
						<small id="email" class="form-text text-muted">We'll never share your email with anyone else.</small>
					</div>
                </div>
                
                <div class="login margin-1">
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" 
                        <?php
                            echo 'value='.$pswd;
                        ?>
                        >
					</div>
                    <div class="form-group">
						<label for="exampleInputPassword2">Confirm your password</label>
						<input type="password" class="form-control" id="exampleInputPassword2" placeholder="Input the same password" name="confirmation" <?php
                            echo 'value='.$pswd;
                        ?>
                        >
					</div>
				</div>
                
				<button type="submit" name="submit" class="btn btn-primary margin-1" id = "submit">Register</button>
                
                <div class="margin-1">
                <?php
                      if (isset($registration)) {
                          switch($registration) {
                              case 0:
                                  echo "This email is already used.";
                                  break;
                              case 2:
                                  echo "The passwords are not the same.";
                                  break;
                              case 1:
                                  echo "You are now registered ! Welcome to Maple Zoo !";
                                  break;
                              default:  
                                  echo "ERREUR : ";
                                  var_dump($registration);
                                  break;
                          }
                      }                  
                ?>
                </div>
                
			</form>
		</div>
	</body>
    
     <script>
        $(document).ready(function(){ var nom= $( "#exampleInputEmail1" ).val(); alert(nom); });
         
         let aUser = new User($( "#exampleInputEmail1" ).val(), $( "#exampleInputPassword1" ).val());
         console.log(aUser);
         
         $(document).ready(function(){
            $("#submit").click(function(){
                saveInSession(aUser.pswd,aUser.mail);
                affItemFromStorage(aUser.mail);
            });
         });
    </script>
    
</html>