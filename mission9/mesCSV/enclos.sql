-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 04, 2022 at 09:57 AM
-- Server version: 5.7.36
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zoo`
--

-- --------------------------------------------------------

--
-- Table structure for table `enclos`
--

DROP TABLE IF EXISTS `enclos`;
CREATE TABLE IF NOT EXISTS `enclos` (
  `zone` char(3) NOT NULL,
  `nom` varchar(40) NOT NULL,
  `capacite` smallint(6) DEFAULT NULL,
  `taille` smallint(6) DEFAULT NULL,
  `eau` tinyint(1) DEFAULT NULL,
  `responsable` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`zone`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enclos`
--

INSERT INTO `enclos` (`zone`, `nom`, `capacite`, `taille`, `eau`, `responsable`) VALUES
('A01', 'La savane', 45, 26500, 0, 3),
('A02', 'Les bois', 50, 15750, 1, 6),
('A03', 'La foret enchantÃ©e', 30, 8600, 0, 7),
('B01', 'Bassin 1', 10, 450, 1, 5),
('B02', 'Bassin 2', 20, 780, 1, 4),
('C01', 'Montagne', 35, 12700, 0, 6),
('SOI', 'Local de soin', 8, 250, NULL, 9),
('DIR', 'Locaux de la direction', NULL, 120, NULL, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
