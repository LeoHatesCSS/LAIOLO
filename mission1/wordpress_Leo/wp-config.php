<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'WordPress' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'mJ1z5aLhiKO)e,#eSQOm,,%vpV@ROI5sQsh]y[DTI.G.pkzyqC0$iH@xAk]E%J^(' );
define( 'SECURE_AUTH_KEY',  '7j3nL6r_Z6x6>u Yv/`S&B,v{I/G[wp=IiU:h/sgi<qKSmY;}|>Wg7UpM<W=RP_u' );
define( 'LOGGED_IN_KEY',    'Z0*jMsc.,F-{lBro98pAKjrK3<VWd*W[a;t-W,@U-<5AfZ{c8nqPrReVE`k^g#$F' );
define( 'NONCE_KEY',        'twHJ@~qqf.xQ( zmV1`o>D<8~;O`aflTn4jTacT0oM8=>v>c{}Ff#AiZx|HTly[U' );
define( 'AUTH_SALT',        'o=>2WT,2FvcsMg:J_~>buRp*}ferZOn^(Q=Ib=WWJG0>WR7S_o6V_[5o_?nY#:W8' );
define( 'SECURE_AUTH_SALT', '&Ka|f>u>I,wklG7C,vBJF<Svh*voZj?Jm`sbji,p_y`n)g11g[(V&o9cDD4{RZIO' );
define( 'LOGGED_IN_SALT',   'V>B81hMd%Iy,6m<PT&<+I~VZw)Zbtl(`?7V6x3V5jS]={2,s&3HEiC}~A7pN+PDA' );
define( 'NONCE_SALT',       'e.VXT%b!Dt=9Tg~n7-n/dKU}E6~y9dnJmlL?OY-$.krPGlAIN0>+39RI=FT2<ex[' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
