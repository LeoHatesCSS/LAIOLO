$(document).ready(function () {
    $('#btn').click(function () {
        let username = $("#login").val();
        let password = $("#exampleInputPassword1").val();
        let faux = $("#faux");
        let vrai = $("#vrai");

        
        faux.html("");
        vrai.html("");

        
        if (username == "") {
            alert("You need to fill in your username");
        }
        
        if (password == "") {
            alert("You need to fill in your password");
        }
        

        $.ajax("./verification.php",{
            type: "POST",
            data: 'login=' + username + '&pswrd=' + password,
            success: function (data, textStatus, jqXHR) {
                console.log(jqXHR.status);
                $("#vrai").html('<div class="alert alert-success" role="alert">Signed in successfully as '+ username + '</div>');
            },
            error: function (xhr,status,error) {
                console.log(xhr.status);
                 $("#faux").html('<div class="alert alert-danger" role="alert">Wrong ID or password!</div>');
            }
              /*success : function(resultat){
                console.log(resultat);
                if (resultat == "1") {
                    ok.html("Logged in successfully");
                }else{
                    error.html("Username or password incorrect !");
                }
              }*/
        });
    });
});