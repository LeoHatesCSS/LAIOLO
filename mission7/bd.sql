--
-- Base de données :  `zootickoon`
--

CREATE DATABASE IF NOT EXISTS zootickoon;

USE zootickoon;

-- --------------------------------------------------------

--
-- Structure de la table `ticket`
--

CREATE TABLE IF NOT EXISTS `ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datet` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `login` varchar(50) NOT NULL,
  `sujet` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  `prio` varchar(10) NOT NULL DEFAULT 'faible',
  `secteur` varchar(10) NOT NULL DEFAULT 'Normal mob', 
  `statut` varchar(10) NOT NULL DEFAULT 'en cours',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `ticket`
--
insert into ticket(id,login, sujet, description, prio, secteur, statut) values(1,"zaza@gmail.com","fuite d'eau","l'eau coule du robinet","moyen","Normal mob", "en cours");